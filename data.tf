data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}

data "aws_route53_zone" "pub" {
  provider = aws.dns
  name         = var.pub_dns_suffix
  private_zone = false
}

data "aws_route53_zone" "priv" {
  provider = aws.dns
  name         = var.priv_dns_suffix
  private_zone = false
}
