#Aviatrix controller vars
variable "aviatrix_admin_account" {
  type    = string
  default = "admin"
}

variable "aviatrix_admin_password" { type = string }
variable "aviatrix_controller_ip" { type = string }

#Regions
variable "aws_region" {
  type    = string
  default = "eu-central-1"
}

variable "azure_region" {
  type    = string
  default = "Japan East"
}

variable "gcp_region" {
  type    = string
  default = "us-east1"
}

#Contoller access accounts
variable "aws_account_name" {
  type    = string
  default = "AWS"
}

variable "azure_account_name" {
  type    = string
  default = "Azure"
}

variable "gcp_account_name" {
  type    = string
  default = "GCP"
}

#CSP Accounts
variable "aws_account_number" { type = string }
variable "aws_access_key" { type = string }
variable "aws_secret_key" { type = string }
variable "azure_subscription_id" { type = string }
variable "azure_directory_id" { type = string }
variable "azure_application_id" { type = string }
variable "azure_application_key" { type = string }
variable "gcp_project_id" { type = string }
variable "gcp_service_account_json" { type = string }

variable "ssh_key" {
  type    = string
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCnNDeCuEOgJjtFFzWa9fXyKj8mSdCnCVR+iOm40JYSO4/kKEOflq0VvtIcnezv1wa4Ghj3RqEcFd9857qAQfqsn5KgjwuoYG37eTthz9waKSbem6l8hilR4CncagBqMqje8EDuWFdyNPWmgM04nHJ+HRn0UoXzYikSbbQJ082XORREEpZA4Rt7ZHtIncqN5EMBPQ4lflDOR7l0pCTcGObHNPOuWje35ZQqcjryskUkgvEzx+kFxnJ5fG2cwvDkoq8JrCwXhZNmoYNvR6cAtzMo7S/v7THxCxYMgsSUWRzY1+Pi93EB/CIZp5le0gewblrzXpc8DmHd5NPi3ObPwPTh dennis@NUC"
}

variable "pod_id" {
    type = string
}

variable "pub_dns_suffix" {
    type = string
    default = "pub.avxlab.nl"
}

variable "priv_dns_suffix" {
    type = string
    default = "priv.avxlab.nl"
}

#AWS Account used for route53 DNS
variable "dns_aws_access_key" { type = string }
variable "dns_aws_secret_key" { type = string }