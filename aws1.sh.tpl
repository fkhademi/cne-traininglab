#! /bin/bash
sudo hostnamectl set-hostname pod${podid}-${name}
sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
sudo echo 'ubuntu:Password123!' | /usr/sbin/chpasswd
sudo /etc/init.d/ssh restart

sleep 3m #Allow internet access to be established. E.g. IGW or NGFW instantiation.
sudo apt update
sudo apt -y install python3-pip
sudo pip3 install webssh

openssl req -newkey rsa:4096 \
                -x509 \
                -sha256 \
                -days 3650 \
                -nodes \
                -out /home/ubuntu/cert.crt \
                -keyout /home/ubuntu/cert.key \
                -subj "/C=NL/ST=Noord-Holland/L=Amsterdam/O=Aviatrix/OU=Avxlab/CN=pod${podid}-aws-srv1.pub.avxlab.nl"

sudo wssh --certfile='/home/ubuntu/cert.crt' --keyfile='/home/ubuntu/cert.key' --sslport=443 --port=80