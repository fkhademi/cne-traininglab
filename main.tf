#Controller accounts
resource "aviatrix_account" "aws" {
  account_name       = var.aws_account_name
  cloud_type         = 1
  aws_iam            = true
  aws_account_number = var.aws_account_number
  aws_access_key     = var.aws_access_key
  aws_secret_key     = var.aws_secret_key
}

resource "aviatrix_account" "azure" {
  account_name        = var.azure_account_name
  cloud_type          = 8
  arm_subscription_id = var.azure_subscription_id
  arm_directory_id    = var.azure_directory_id
  arm_application_id  = var.azure_application_id
  arm_application_key = var.azure_application_key
}

resource "local_file" "gcp_json" {
  content  = var.gcp_service_account_json
  filename = "gcp.json"
}

resource "aviatrix_account" "gcp" {
  account_name                        = var.gcp_account_name
  cloud_type                          = 4
  gcloud_project_id                   = var.gcp_project_id
  gcloud_project_credentials_filepath = local_file.gcp_json.filename
}

#Transits
module "transit_azure" {
  source  = "terraform-aviatrix-modules/azure-transit/aviatrix"
  version = "2.0.0"

  cidr          = "10.${var.pod_id}2.0.0/20"
  region        = var.azure_region
  account       = aviatrix_account.azure.account_name
  ha_gw         = false
  instance_size = "Standard_B2s"
  name          = "azure-transit"
  prefix        = false
  suffix        = false
}

module "transit_gcp" {
  source  = "terraform-aviatrix-modules/gcp-transit/aviatrix"
  version = "2.0.0"

  cidr    = "10.${var.pod_id}3.0.0/20"
  account = aviatrix_account.gcp.account_name
  region  = var.gcp_region
  ha_gw   = false
  name    = "gcp-transit"
  prefix  = false
  suffix  = false
}

resource "aws_route53_record" "gcp_trans" {
  provider = aws.dns
  zone_id  = data.aws_route53_zone.pub.zone_id
  name     = "pod${var.pod_id}-tgw.${data.aws_route53_zone.pub.name}"
  type     = "A"
  ttl      = "300"
  records  = [module.transit_gcp.transit_gateway.eip]
}

#Spokes
module "gcp_spoke_1" {
  source  = "terraform-aviatrix-modules/gcp-spoke/aviatrix"
  version = "2.0.1"

  name       = "gcp-spoke1"
  cidr       = "10.${var.pod_id}3.100.0/24"
  account    = aviatrix_account.gcp.account_name
  region     = var.gcp_region
  transit_gw = module.transit_gcp.transit_gateway.gw_name
  ha_gw      = false
  prefix     = false
  suffix     = false
}

module "azure_spoke_1" {
  source  = "terraform-aviatrix-modules/azure-spoke/aviatrix"
  version = "2.0.0"

  name          = "azure-spoke1"
  cidr          = "10.${var.pod_id}2.100.0/24"
  region        = var.azure_region
  account       = aviatrix_account.azure.account_name
  transit_gw    = module.transit_azure.transit_gateway.gw_name
  ha_gw         = false
  instance_size = "Standard_B2s"
  prefix        = false
  suffix        = false
}

resource "aviatrix_vpc" "aws1" {
  cloud_type           = 1
  region               = var.aws_region
  account_name         = aviatrix_account.aws.account_name
  name                 = "aws-spoke1"
  cidr                 = "10.${var.pod_id}1.100.0/24"
  aviatrix_transit_vpc = false
  aviatrix_firenet_vpc = false
}

resource "aviatrix_vpc" "aws2" {
  cloud_type           = 1
  region               = var.aws_region
  account_name         = aviatrix_account.aws.account_name
  name                 = "aws-spoke2"
  cidr                 = "10.${var.pod_id}1.101.0/24"
  aviatrix_transit_vpc = false
  aviatrix_firenet_vpc = false
}

#Transit peering
resource "aviatrix_transit_gateway_peering" "azure_gcp" {
  transit_gateway_name1 = module.transit_azure.transit_gateway.gw_name
  transit_gateway_name2 = module.transit_gcp.transit_gateway.gw_name
}
