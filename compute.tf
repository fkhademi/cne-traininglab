#AWS Test Servers
resource "aws_key_pair" "key" {
  key_name   = "pod${var.pod_id}-key"
  public_key = var.ssh_key
}

#AWS1
resource "aws_security_group" "aws1" {
  name   = "aws1-all-traffic"
  vpc_id = aviatrix_vpc.aws1.vpc_id

  ingress {
    from_port   = 0
    to_port     = 22
    protocol    = "6"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0
    to_port     = 443
    protocol    = "6"
    cidr_blocks = ["0.0.0.0/0"]
  }  

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "1"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "template_file" "aws1" {
  template = file("${path.module}/aws1.sh.tpl")

  vars = {
    podid = var.pod_id
    name = "aws-srv1"
  }
}

resource "aws_instance" "aws1" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.key.key_name
  subnet_id                   = aviatrix_vpc.aws1.subnets[length(aviatrix_vpc.aws1.subnets) / 2].subnet_id
  associate_public_ip_address = true
  security_groups             = [aws_security_group.aws1.id]
  user_data                   = data.template_file.aws1.rendered
  lifecycle {
    ignore_changes = [security_groups]
  }
  tags = {
    Name = "pod${var.pod_id}-aws1-srv"
  }
}

resource "aws_route53_record" "aws1_pub" {
  provider = aws.dns
  zone_id  = data.aws_route53_zone.pub.zone_id
  name     = "pod${var.pod_id}-aws-srv1.${data.aws_route53_zone.pub.name}"
  type     = "A"
  ttl      = "300"
  records  = [aws_instance.aws1.public_ip]
}

resource "aws_route53_record" "web_host" {
  provider = aws.dns
  zone_id  = data.aws_route53_zone.pub.zone_id
  name     = "pod${var.pod_id}-web.${data.aws_route53_zone.pub.name}"
  type     = "CNAME"
  ttl      = "300"
  records  = [aws_route53_record.aws1_pub.name]
}

resource "aws_route53_record" "aws1_priv" {
  zone_id = data.aws_route53_zone.priv.zone_id
  name    = "pod${var.pod_id}-aws-srv1.${data.aws_route53_zone.priv.name}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.aws1.private_ip]
}

#AWS2
resource "aws_security_group" "aws2" {
  name   = "aws2-all-traffic"
  vpc_id = aviatrix_vpc.aws2.vpc_id

  ingress {
    from_port   = 0
    to_port     = 22
    protocol    = "6"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0
    to_port     = 443
    protocol    = "6"
    cidr_blocks = ["0.0.0.0/0"]
  }  

  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "template_file" "aws2" {
  template = file("${path.module}/other.sh.tpl")

  vars = {
    podid = var.pod_id
    name = "aws-srv2"
  }
}

resource "aws_instance" "aws2" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.key.key_name
  subnet_id                   = aviatrix_vpc.aws2.subnets[length(aviatrix_vpc.aws2.subnets) / 2].subnet_id
  associate_public_ip_address = true
  security_groups             = [aws_security_group.aws2.id]
  user_data                   = data.template_file.aws2.rendered
  lifecycle {
    ignore_changes = [security_groups]
  }
  tags = {
    Name = "pod${var.pod_id}-aws2-srv"
  }
}

resource "aws_route53_record" "aws2_pub" {
  provider = aws.dns
  zone_id  = data.aws_route53_zone.pub.zone_id
  name     = "pod${var.pod_id}-aws-srv2.${data.aws_route53_zone.pub.name}"
  type     = "A"
  ttl      = "300"
  records  = [aws_instance.aws2.public_ip]
}

resource "aws_route53_record" "aws2_priv" {
  zone_id = data.aws_route53_zone.priv.zone_id
  name    = "pod${var.pod_id}-aws-srv2.${data.aws_route53_zone.priv.name}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.aws2.private_ip]
}

#Azure testserver
resource "azurerm_resource_group" "compute" {
  name     = "pod${var.pod_id}-compute"
  location = var.azure_region
}

resource "azurerm_public_ip" "azure1" {
  name                = "azure1"
  location            = var.azure_region
  resource_group_name = azurerm_resource_group.compute.name
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "azure1" {
  name                = "azure1-nic1"
  location            = var.azure_region
  resource_group_name = azurerm_resource_group.compute.name

  ip_configuration {
    name                          = "azure1-nic1"
    subnet_id                     = module.azure_spoke_1.vnet.subnets[0].subnet_id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.azure1.id
  }
}

resource "azurerm_virtual_machine" "azure1" {
  name                  = "azure1"
  location              = var.azure_region
  resource_group_name   = azurerm_resource_group.compute.name
  network_interface_ids = [azurerm_network_interface.azure1.id]
  vm_size               = "Standard_B2s"

  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
  storage_os_disk {
    name              = "pod${var.pod_id}-azure1-myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "pod${var.pod_id}-azure1"
    admin_username = "ubuntu"
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
      path     = "/home/ubuntu/.ssh/authorized_keys"
      key_data = var.ssh_key
    }
  }
}

resource "aws_route53_record" "azure1_priv" {
  zone_id = data.aws_route53_zone.priv.zone_id
  name    = "pod${var.pod_id}-azure-srv1.${data.aws_route53_zone.priv.name}"
  type    = "A"
  ttl     = "300"
  records = [azurerm_network_interface.azure1.private_ip_address]
}

/*
resource "aws_route53_record" "azure1_pub" {
  provider = aws.dns
  zone_id    = data.aws_route53_zone.pub.zone_id
  name       = "pod${var.pod_id}-azure-srv1.${data.aws_route53_zone.pub.name}"
  type       = "A"
  ttl        = "300"
  records    = [azurerm_public_ip.azure1.ip_address]
  depends_on = [azurerm_virtual_machine.azure1, ] #Public IP is only assigned by Azure after NIC is bound to instance  
}
*/

#Google Test VM
resource "google_compute_instance" "gcp_srv1" {
  name         = "pod${var.pod_id}-gcp1-srv"
  machine_type = "f1-micro"
  zone         = "${var.gcp_region}-b"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1604-lts"
    }
  }

  network_interface {
    network    = module.gcp_spoke_1.vpc.name
    subnetwork = module.gcp_spoke_1.vpc.subnets[0].name
    access_config {
      // Include this section to give the VM an external ip address
    }
  }

  tags = ["allow-icmp-ssh"]
  metadata = {
    ssh-keys = "ubuntu:${var.ssh_key}"
  }
}

# GCP FW Rule
resource "google_compute_firewall" "gcp_fw_rule" {
  name    = "allow-icmp-ssh"
  network = module.gcp_spoke_1.vpc.name

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  allow {
    protocol = "icmp"
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["allow-icmp-ssh"]
}

# GCP Server DNS Record
resource "aws_route53_record" "gcp1_pub" {
  provider = aws.dns
  zone_id  = data.aws_route53_zone.pub.zone_id
  name     = "pod${var.pod_id}-gcp-srv1.${data.aws_route53_zone.pub.name}"
  type     = "A"
  ttl      = "300"
  records  = [google_compute_instance.gcp_srv1.network_interface.0.access_config.0.nat_ip]
}

resource "aws_route53_record" "gcp1_priv" {
  provider = aws.dns
  zone_id  = data.aws_route53_zone.priv.zone_id
  name     = "pod${var.pod_id}-gcp-srv1.${data.aws_route53_zone.priv.name}"
  type     = "A"
  ttl      = "300"
  records  = [google_compute_instance.gcp_srv1.network_interface.0.network_ip]
}
